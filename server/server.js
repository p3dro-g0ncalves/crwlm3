import express from 'express';
import crawlerService from './crawler.service';

const port = 8888;
const app = express();

app.use(express.static(`${__dirname}./../public`));
app.use('/static', express.static('build'));

app.use('/coverage', express.static('coverage/lcov-report'));

app.listen(port, () => {
    console.log(`listening on ${port}`);
    console.log(`$ open http://localhost:${port}`);
    // opn(`http://localhost:${port}`);
});

app.get('/get-contents/url/:url/hops/:hops?', (req, res) => {
    const url = new Buffer(req.params.url, 'base64').toString('utf8').trim();
    const crawler = new crawlerService(url);

    if (crawler.isValid()) {
        crawler.parseContents()
            .then(parse => {
                res.send({
                    success: true,
                    data: parse
                });
            }, () => {
                res.send({
                    success: false
                });
            });
    } else {
        res.send({
            success: false,
            request: {
                url: crawler.getOriginalUrl()
            },
            parse: crawler.getParseInfo()
        });
    }
});

app.get('/download/url/:url/data/:data', (req, res) => {
    const data = new Buffer(req.params.data, 'base64').toString('utf8').trim();

    try {
        res.setHeader('Content-disposition', `attachment; filename=${req.params.url}.json`);
        res.setHeader('Content-type', 'application/json');
        res.charset = 'UTF-8';
        res.write(data);
        res.end();
    }
    catch (e) {
        res.send(e);
    }

});

