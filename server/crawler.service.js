import url from 'url';
import http from 'http';
import https from 'https';
import extractor from 'oust';

export default requestedUrl => {
    const parsedUrl = url.parse(requestedUrl);

    const _fetchData = (url) => {
        const _onResponse = (response, resolve, reject) => {
            const hasResponseFailed = response.status >= 400;
            let responseBody = '';

            if (hasResponseFailed) {
                reject(`Request to ${response.url} failed with HTTP ${response.status}`);
            }
            response.on('data', chunk => responseBody += chunk.toString());
            response.on('end', () => resolve(responseBody));
        };

        return new Promise((resolve, reject) => {
            const requestProtocol = parsedUrl.protocol === 'https:' ? https : http;
            const request = requestProtocol.get(url, res => _onResponse(res, resolve, reject));

            request.on('error', reject);
            request.end();
        });
    };

    return {
        isValid() {
            return !!parsedUrl.hostname || false;
        },
        getOriginalUrl() {
            return parsedUrl.href;
        },
        getParseInfo() {
            return parsedUrl;
        },
        getContents() {
            if (this.isValid()) {
                return _fetchData(parsedUrl.href);
            }
        },
        parseContents() {
            return new Promise((resolve, reject) => {
                this.getContents().then(html => {
                    if (html !== '') {
                        resolve({
                            stylesheets: extractor(html, 'stylesheets'),
                            scripts: extractor(html, 'scripts'),
                            href: extractor(html, 'links'),
                            images: extractor(html, 'images')
                        });
                    } else {
                        reject({success: false});
                    }
                }, error => {
                    reject(error);
                });
            });
        }
    };
};