const path = require('path');
const webpack = require('webpack');
const UglifyJSPlugin = require('uglifyjs-webpack-plugin');
const WebpackNotifierPlugin = require('webpack-notifier');
const ExtractTextPlugin = require('extract-text-webpack-plugin');

module.exports = {
    context: __dirname + '/app',
    entry: {
        app: ['./app.js']
    },
    output: {
        path: __dirname + '/build',
        filename: '[name].bundle.js',
        devtoolLineToLine: true,
        sourceMapFilename: '[name].bundle.js.map'
    },
    module: {
        loaders: [
            {
                include: [path.resolve('./app')],
                test: /\.js$/, loader: 'babel-loader'
            },
            {
                include: [path.resolve('./app')],
                test: /\.html$/, loader: "html-loader"
            },
            {
                test: /\.(css)$/,
                include: [path.resolve('./app')],
                loader: ExtractTextPlugin.extract({
                    use: [{
                        loader: 'css-loader',
                        options: {
                            modules: true,
                            sourceMap: true,
                            camelCase: true,
                            importLoaders: true,
                            minimize: true
                        }
                    }, 'postcss-loader'],
                    fallback: 'style-loader',
                }),
            },
            {
                test: /\.(css)$/,
                include: [path.resolve('./node_modules/jsonformatter')],
                loader: ExtractTextPlugin.extract({
                    use: ['css-loader', 'postcss-loader'],
                    fallback: 'style-loader',
                }),
            },
            {
                test: /\.woff(2)?(\?v=[0-9]\.[0-9]\.[0-9])?$/,
                include: [path.resolve('./node_modules/font-awesome-webpack/')],
                loader: "url-loader?limit=10000&mimetype=application/font-woff"
            },
            {
                test: /\.(ttf|eot|svg)(\?v=[0-9]\.[0-9]\.[0-9])?$/,
                include: [path.resolve('./node_modules/font-awesome-webpack/')],
                loader: "file-loader"
            }
        ]
    },
    plugins: [
        new webpack.NoEmitOnErrorsPlugin(),
        new WebpackNotifierPlugin({
            title: 'Webpack',
            excludeWarnings: true,
            alwaysNotify: false
        }),
        new webpack.optimize.UglifyJsPlugin({
            compress: {
                warnings: false
            }
        }),
        new ExtractTextPlugin({filename: '[name].bundle.css', allChunks: true}),
        new UglifyJSPlugin(),
    ],
    devtool: 'source-map',
};