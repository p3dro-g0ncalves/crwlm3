import angular from 'angular';
import 'jsonformatter/';
import './app.css';
import 'jsonformatter/dist/json-formatter.min.css';

angular.module('crwlm3', ['jsonFormatter']);

import './components/mainHeader/index';
import './components/crawler/index';