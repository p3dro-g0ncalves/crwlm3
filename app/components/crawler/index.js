import angular from 'angular';
import crawlerHTML from './crawler.template';

import './crawler.service';
import './crawler.controller';

export default angular
    .module('crwlm3')
    .directive('crawler', () => ({
        template: crawlerHTML,
        controller: 'CrawlerController',
        controllerAs: 'crawler'
    }));






