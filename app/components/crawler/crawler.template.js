'use strict';

import styles from './index.css';

const formHTML =
    `<form class=${styles.formHolder} 
           ng-class="{${styles.formHolderCentered}: !crawler.fetchSuccess, ${styles.formHolderResults}: crawler.fetchSuccess}"
           ng-submit="crawler.events.submitCrawler()">
        <input id="model-url" type="text" name="url" ng-model="crawler.model.url" class=${styles.formHolderInput} />
        <button type="submit" class=${styles.formHolderSubmit}>GO</button>
    </form>`;

const resultOptions =
    `<div class=${styles.jsonOptions}>
        <button class=${styles.jsonOptionsButton} ng-click="crawler.events.copyData()">
            <i class=${styles.jsonOptionsCopy}></i>
        </button>
        <button class=${styles.jsonOptionsButton} ng-click="crawler.events.downloadJson()">
            <i class=${styles.jsonOptionsDownload}></i>
        </button>
        <button class=${styles.jsonOptionsButton} ng-click="crawler.events.reset()">
            <i class=${styles.jsonOptionsClose}></i>
        </button>
    </div>`;

const resultHTML =
    `<div ng-if="crawler.fetchSuccess" class=${styles.jsonContainer}>
        ${resultOptions}
        <div class=${styles.jsonHolder}>
            <json-formatter class="json-formatter-dark" open="2" json="crawler.fetchedData"></json-formatter>  
        </div>
    </div>`;

const crawlerHTML = () => formHTML + resultHTML;

export default crawlerHTML;