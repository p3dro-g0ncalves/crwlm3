import angular from 'angular';
import base64 from 'base-64';

export default angular.module('crwlm3')
    .service('CrawlerService', ['$q', '$http', function ($q, $http) {
        this.base64Encode = url => base64.encode(url.toString());
        this.urlEncodeEncode = string => encodeURIComponent(string);

        this.getUrlContents = url => {
            const deferred = $q.defer();

            $http.get(`/get-contents/url/${this.urlEncodeEncode(this.base64Encode(url))}/hops/1`)
                .then(response => {
                    deferred.resolve(response.data || {success: false});
                }, function errorCallback(e) {
                    deferred.reject({success: false, error: e});
                });

            return deferred.promise;
        };
    }]);