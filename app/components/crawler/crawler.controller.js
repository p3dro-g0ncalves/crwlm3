import angular from 'angular';
import styleUtils from '../../utils.css';

export default angular.module('crwlm3')
    .controller('CrawlerController', [
        '$scope',
        '$rootScope',
        '$window',
        '$filter',
        '$timeout',
        'CrawlerService',
        function ($scope, $rootScope, $window, $filter, $timeout, CrawlerService) {
            const vm = this;

            vm.fetchedData = null;
            vm.fetchSuccess = false;
            vm.fetchedUrl = null;

            $rootScope.loadingClass = styleUtils.loading;
            const _loading = isLoading => {
                $rootScope.loading = isLoading;
            };

            const _submitCrawler = () => {
                _loading(true);
                CrawlerService.getUrlContents(vm.model.url)
                    .then(function (fetchedData) {
                        vm.fetchSuccess = fetchedData.success;
                        if (fetchedData.success) {
                            vm.fetchedData = fetchedData.data;
                            vm.fetchedUrl = vm.model.url;
                        }
                    }, function (error) {
                        vm.fetchSuccess = error.success;
                    })
                    .finally(function () {
                        _loading(false);
                    });
            };

            const _copyData = () => {
                try {
                    document.oncopy = function (event) {
                        event.clipboardData.setData('Text', $filter('json')(vm.fetchedData));
                        event.preventDefault();
                    };
                    document.execCommand('copy');
                    document.oncopy = undefined;
                } catch (e) {
                    throw {
                        message: 'Browser doesnt support copy',
                        error: e
                    };
                }
            };

            const _downloadJson = () => {
                const fileName = CrawlerService.urlEncodeEncode(vm.fetchedUrl);
                const data = CrawlerService.urlEncodeEncode(CrawlerService.base64Encode($filter('json')(vm.fetchedData)));

                $window.open(`/download/url/${fileName}/data/${data}`, '_blank');
            };

            const _selectInput = (selectInput) => {
                selectInput = angular.isUndefined(selectInput) ? true : selectInput;

                $timeout(() => {
                    const element = $window.document.getElementById('model-url');
                    if (element) {
                        if (selectInput) {
                            element.select();
                        }
                        element.focus();
                    }
                });
            };

            const _reset = () => {
                vm.fetchedData = null;
                vm.fetchSuccess = false;
                vm.fetchedUrl = null;

                _selectInput();
            };

            vm.model = {
                url: 'https://www.gocardless.com/'
            };

            vm.events = {
                submitCrawler: _submitCrawler,
                copyData: _copyData,
                downloadJson: _downloadJson,
                reset: _reset
            };

            ((() => {
                _selectInput(false);
            })());
        }
    ]);