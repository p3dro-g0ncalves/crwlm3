import styles from './index.css';

const mainHeaderHTML = () =>
    `<header class=${styles.header}>
        <h1>Crwlm3</h1>
        <div>CRLM3 - The simple, yet awesome, web crawler</div>
    </header>`;

export default mainHeaderHTML;
