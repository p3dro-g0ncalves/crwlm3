import angular from 'angular';
import mainHeaderHTML from './mainHeader.template';

export default angular.module('crwlm3')
    .directive('mainHeader', () => ({
        template: mainHeaderHTML
    }));