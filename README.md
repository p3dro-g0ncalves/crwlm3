# README // CRWLM3 - Simple, yet awesome, web crawler #

Built with Angular [ https://angularjs.org/ ]

Powered by Node-Express [ http://expressjs.com ] and Nodemon [ https://nodemon.io/ ]

Transpiled from ES6 with Babel [ https://babeljs.io ]

Transpiled from CSSNext [ http://cssnext.io/ ] with Post-Css [ http://postcss.org/ ]

Bundled with webpack [ https://webpack.js.org ]

## Dependencies ##

You need to install Node.js and then the development tools. Node.js comes with a package manager called [npm](http://npmjs.org) for installing NodeJS applications and libraries.

Consider alto installing [Yarn](https://yarnpkg.com/). Please check the [installation](https://yarnpkg.com/lang/en/docs/install/) steps.

## Instructions ##

### Install Project ###

`$ npm install` | `$ yarn install`

### Run project ###

*Development Mode*

* `$ npm run start:dev` | `$ yarn start:dev`
* Access http://localhost:8888 or run `$ open http://localhost:8888`

*Non-Development Mode*

* `$ npm run start` | `$ yarn start`
* Access http://localhost:8888 or run `$ open http://localhost:8888`


### Compile client ###

*Development Mode*

 * `$ npm run compile:dev` | `$ yarn compile:dev`


*Non-Development Mode*

 * `$ npm run compile` | `$ yarn compile`

### Test client ###

Runs static tests (eslint and stylelint) and unit tests (jest)

 * `npm run test` | `yarn test`
 * check coverage report in `http://localhost:8888/coverage/`

### Available Commands ###

 * `npm install` | `yarn install` to install project dependencies **(mandatory)**
 * `npm run clean` | `yarn clean` deletes build folder
 * `npm run eslint` | `yarn eslint` to run javascript static validation
 * `npm run eslint:fix` | `yarn eslint:fix` to run javascript static validation and fix errors
 * `npm run stylelint` | `yarn stylelint` to run css static validation
 * `npm run stylelint:fix` | `yarn eslint:fix` to run css static validation and fix errors
 * `npm run static-test` | `yarn static-test` to run all static validation and fix errors
 * `npm run unit` | `yarn unit` to run the jest unit tests
 * `npm run test` | `yarn test` to run all tests
 * `npm run compile` | `yarn compile` to bundle files with a clean directory
 * `npm run compile:dev` | `yarn compile` to bundle files
 * `npm run serve` | `yarn compile` to start the server
 * `npm run start` | `yarn start` to bundle the application with a clean directory and start the server
 * `npm run start:dev` | `yarn start:dev` to bundle the application and start the server